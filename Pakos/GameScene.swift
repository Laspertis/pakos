//
//  GameScene.swift
//  Pakos
//
//  Created by Melania Conte on 14/03/2020.
//  Copyright © 2020 Melania Conte. All rights reserved.
//

import SpriteKit
import GameplayKit

enum CategoryMask: UInt32 {
  case pako = 1
  case virus = 2
  case cake = 4
  case bin = 8
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    private var object: SKSpriteNode?
    private var pako: SKSpriteNode?
    private var bin: SKSpriteNode?
    
    var touchPoint: CGPoint = CGPoint()
    var touching: Bool = false
    
    private var restart: SKLabelNode?
    private var label: SKLabelNode?
    private var countDownLabel: SKLabelNode?
    private var count: Int?
    
    override func sceneDidLoad() {
        physicsWorld.contactDelegate = self
    }
    
    var timer: Timer?
     var totalTime = 60

     private func startOtpTimer() {
            self.totalTime = 60
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        }

    @objc func updateTimer() {
            print(self.totalTime)
            self.countDownLabel!.text = self.timeFormatted(self.totalTime) // will show timer
            if totalTime != 0 {
                totalTime -= 1
            } else {
                if let timer = self.timer {
                    timer.invalidate()
                    self.timer = nil
                }
            }
        }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    override func didMove(to view: SKView) {
        setUpScene()
        gameUpdate()
        startOtpTimer()
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        guard let nodeA = contact.bodyA.node else { return }
        guard let nodeB = contact.bodyB.node else { return }

        if nodeA.name == "pako" {
            collisionBetween(pako: nodeA, object: nodeB)
        } else if nodeA.name == "bin" {
            collisionBetween(bin: nodeA, object: nodeB)
        }
    }
    
    func setUpScene(){
        
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.count = 0
        let str = "\(count ?? 0)"
        self.label = SKLabelNode(text: str)
        self.label?.color = .black
        self.label?.setScale(2)
        self.label?.position = CGPoint(x: -(self.scene?.frame.width)!/2 + (label?.frame.size.width)! * 3, y: (self.scene?.frame.height)!/2 - (label?.frame.size.height)! * 2)
        self.addChild(label!)
        
        self.countDownLabel = SKLabelNode(text: "01:00")
        self.countDownLabel?.color = .black
        self.countDownLabel?.setScale(2)
        self.countDownLabel?.position = CGPoint(x: (self.scene?.frame.width)!/2 - (countDownLabel?.frame.size.width)! , y: (self.scene?.frame.height)!/2 - (countDownLabel?.frame.size.height)! * 2)
        self.addChild(countDownLabel!)
        
        self.pako = SKSpriteNode(imageNamed: "pako")
        self.pako?.name = "pako"
        self.pako?.scale(to: CGSize(width:  230, height: 230))
        self.pako?.physicsBody?.pinned = true
        self.pako?.position = CGPoint(x: 0, y: (self.scene?.frame.size.height)!/3)
        self.pako?.zPosition = 10
        self.pako?.isUserInteractionEnabled = false
        self.pako?.physicsBody = SKPhysicsBody(circleOfRadius: (self.pako?.frame.width)!/2)
        self.pako?.physicsBody?.affectedByGravity = false
        self.pako?.physicsBody?.isDynamic = false
        self.pako?.physicsBody?.collisionBitMask = 1
        self.pako?.physicsBody?.categoryBitMask = CategoryMask.pako.rawValue
        self.pako?.physicsBody?.collisionBitMask = 7
        self.pako?.physicsBody?.contactTestBitMask = 14
        self.addChild(pako!)
        
        self.bin = SKSpriteNode(imageNamed: "bin")
        self.bin?.name = "bin"
        self.bin?.scale(to: CGSize(width:  230, height: 230))
        self.bin?.physicsBody?.pinned = true
        self.bin?.position = CGPoint(x: 0, y: -(self.scene?.frame.size.height)!/2 + bin!.frame.size.height/4.5)
        self.bin?.zPosition = 10
        self.bin?.isUserInteractionEnabled = false
        self.bin?.physicsBody = SKPhysicsBody(circleOfRadius: (self.pako?.frame.width)!/3)
        self.bin?.physicsBody?.affectedByGravity = false
        self.bin?.physicsBody?.isDynamic = false
        self.bin?.physicsBody?.collisionBitMask = 1
        self.bin?.physicsBody?.categoryBitMask = CategoryMask.bin.rawValue
        self.bin?.physicsBody?.collisionBitMask = 7
        self.bin?.physicsBody?.contactTestBitMask = 14
        self.addChild(bin!)
    }
    
    func gameUpdate(){
        let lower : UInt32 = 1
        let upper : UInt32 = 100
        let randomNumber = arc4random_uniform(upper - lower) + lower
        if (randomNumber % 10) == 0 {
            var virus: SKSpriteNode?
            virus = SKSpriteNode(imageNamed: "virus")
            virus?.name = "virus"
            virus?.scale(to: CGSize(width: 130, height: 130))
            virus?.position = CGPoint(x: 0, y: -(self.scene?.frame.size.height)!/4)
            virus?.zPosition = 10
            
            virus?.physicsBody?.mass = 0.5
            virus?.physicsBody?.density = 0.5
            virus?.physicsBody = SKPhysicsBody(circleOfRadius: (virus!.frame.width)/3)
            virus?.physicsBody?.angularDamping = 0
            virus?.physicsBody?.affectedByGravity = false
            virus?.physicsBody?.isDynamic = true
            
            virus?.physicsBody?.categoryBitMask = CategoryMask.virus.rawValue
            virus?.physicsBody?.collisionBitMask = ~(CategoryMask.pako.rawValue | CategoryMask.cake.rawValue | CategoryMask.bin.rawValue)
            virus?.physicsBody?.contactTestBitMask = CategoryMask.virus.rawValue
            self.object = virus
            self.addChild(object!)
        }
        else{
            var cake: SKSpriteNode?
            cake = SKSpriteNode(imageNamed: "cake")
            cake?.name = "cake"
            cake?.scale(to: CGSize(width: 130, height: 130))
            cake?.position = CGPoint(x: 0, y: -(self.scene?.frame.size.height)!/4)
            cake?.zPosition = 10
            
            cake?.physicsBody?.mass = 0.5
            cake?.physicsBody?.density = 0.5
            cake?.physicsBody = SKPhysicsBody(circleOfRadius: (cake?.frame.width)!/3)
            cake?.physicsBody?.angularDamping = 0
            cake?.physicsBody?.affectedByGravity = false
            cake?.physicsBody?.isDynamic = true
            
            cake?.physicsBody?.categoryBitMask = CategoryMask.cake.rawValue
            cake?.physicsBody?.collisionBitMask = ~(CategoryMask.pako.rawValue | CategoryMask.virus.rawValue | CategoryMask.bin.rawValue)
            cake?.physicsBody?.contactTestBitMask = CategoryMask.cake.rawValue
            self.object = cake
            self.addChild(object!)
        }
    }
    
    func gameOver(){
        let label = SKLabelNode(text: "GAME OVER")
        label.position = CGPoint(x: 0, y: 0 - label.frame.height)
        label.fontSize = 100
        self.addChild(label)
        restart = SKLabelNode(text: "RESTART")
        restart!.position = CGPoint(x: 0, y: 0 - label.frame.height - restart!.frame.height)
        restart?.fontSize = 100
        self.addChild(restart!)
        self.timer?.invalidate()
//        countDownLabel?.text = "00:00"
    }
    
    func collisionBetween(bin: SKNode, object: SKNode) {
        if object.name == "cake" {
            print("Collision bin with cake")
            destroy(object: object)
            gameUpdate()
        } else if object.name == "virus" {
            print("Collision with virus")
            destroy(object: object)
            gameUpdate()
        }
    }
    
    func collisionBetween(pako: SKNode, object: SKNode) {
        if object.name == "cake" {
            print("Collision with cake")
            count! += 1
            let str = "\(count ?? 0)"
            self.label?.text = str
            destroy(object: object)
            gameUpdate()
        } else if object.name == "virus" {
            print("Collision with virus")
            destroy(object: object)
            gameOver()
        }
    }

    func destroy(object: SKNode) {
        object.removeFromParent()
    }
    
    func touchDown(atPoint pos : CGPoint) {
    }
    
    func touchMoved(toPoint pos : CGPoint) {
    }
    
    func touchUp(atPoint pos : CGPoint) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self)
        
        if self.object!.frame.contains(location) {
            touchPoint = location
            touching = true
        } else if self.restart != nil {
            if self.restart!.frame.contains(location){
            removeAllChildren()
            setUpScene()
            gameUpdate()
            startOtpTimer()
//            for t in touches {
//                self.touchDown(atPoint: t.location(in: self))
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self)
        touchPoint = location
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        touching = false
//        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if touching {
            let dt:CGFloat = 1.0/40.0
            let distance = CGVector(dx: touchPoint.x-object!.position.x, dy: touchPoint.y-object!.position.y)
            let velocity = CGVector(dx: distance.dx/dt, dy: distance.dy/dt)
            object!.physicsBody!.velocity=velocity
        }
    }
}


